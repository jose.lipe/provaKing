# Slim 3 Skeleton

This is a simple skeleton project for Slim 3 that includes Twig, Flash messages and Monolog.

## Create your project:

    $ composer create-project --no-interaction --stability=dev akrabat/slim3-skeleton my-app

### Run it:

1. `$ cd my-app`
2. `$ php -S 0.0.0.0:8888 -t public public/index.php`
3. Browse to http://localhost:8888

## Key directories

* `app`: Application code
* `app/src`: All class files within the `App` namespace
* `app/templates`: Twig template files
* `cache/twig`: Twig's Autocreated cache files
* `log`: Log files
* `public`: Webserver root
* `vendor`: Composer dependencies

## Key files

* `public/index.php`: Entry point to application
* `app/settings.php`: Configuration
* `app/dependencies.php`: Services for Pimple
* `app/middleware.php`: Application middleware
* `app/routes.php`: All application routes are here
* `app/src/Action/HomeAction.php`: Action class for the home page
* `app/templates/home.twig`: Twig template file for the home page

## Informações do projeto:

	Para utilizar o projeto, você deve usar a versão 7.1 do PHP
	Você deve utilizar o comando composer install, para instalar as dependecias e libs usadas no projeto
	Para a configuração do bando de dados, você deve editar o arquivo /app/settings apartir da linha 74.
	No sistema, apenas usuários admin, que podem gerenciar novos usuários.
	O sistema já vem com dois usuários cadastrados:
	login - admin , senha - admin123
	login - jose.felipe, senha - jose123 


