<?php
// Routes

// Login
$app->get('/', 'App\Action\LoginAction:show')->setName('login.index')->add($auth);
$app->post('/Logar', 'App\Action\LoginAction:logar')->setName('login.logar')->add($auth);
$app->get('/Deslogar', 'App\Action\LoginAction:deslogar')->setName('login.deslogar');


// Register
$app->get('/Registrar', 'App\Action\RegisterAction:show')->setName('register.index')->add($auth);
$app->post('/Registrar/Insert', 'App\Action\RegisterAction:insert')->setName('insert.index')->add($auth);

// Home
$app->get('/Home', 'App\Action\HomeAction:show')->setName('homepage')->add($guardian);


// Usuários
$app->get('/Usuarios', 'App\Action\UserAction:list')->setName('user.list')->add($guardian);
$app->get('/Usuarios/Edit/{id}', 'App\Action\UserAction:edit')->setName('user.edit')->add($guardian);
$app->post('/Usuarios/Edit/Update/{id}', 'App\Action\UserAction:update')->setName('user.update')->add($guardian);
$app->get('/Usuarios/Delete/{id}', 'App\Action\UserAction:delete')->setName('user.delete')->add($guardian);

//Salas
$app->get('/Salas/Cadastro', 'App\Action\SalaAction:show')->setName('sala.show')->add($guardian);
$app->post('/Salas/Insert', 'App\Action\SalaAction:insert')->setName('sala.insert')->add($guardian);
$app->get('/Salas', 'App\Action\SalaAction:list')->setName('sala.list')->add($guardian);
$app->get('/Salas/Edit/{id}', 'App\Action\SalaAction:edit')->setName('sala.edit')->add($guardian);
$app->post('/Salas/Edit/Update/{id}', 'App\Action\SalaAction:update')->setName('sala.update')->add($guardian);
$app->get('/Salas/Delete/{id}', 'App\Action\SalaAction:delete')->setName('sala.delete')->add($guardian);

//Reserva
$app->get('/Agenda/{id}', 'App\Action\AgendaAction:show')->setName('agenda.show')->add($guardian);
$app->get('/Agenda/Store/{id}', 'App\Action\AgendaAction:store')->setName('agenda.store')->add($guardian);
$app->post('/Agenda/Store/Insert', 'App\Action\AgendaAction:insert')->setName('agenda.insert')->add($guardian);
$app->get('/Agenda/Edit/{id}', 'App\Action\AgendaAction:edit')->setName('agenda.edit')->add($guardian);
$app->post('/Agenda/Edit/Update/{id}', 'App\Action\AgendaAction:update')->setName('agenda.update')->add($guardian);
$app->get('/Agenda/Edit/Delete/{id}', 'App\Action\AgendaAction:delete')->setName('agenda.delete')->add($guardian);



