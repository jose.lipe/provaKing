<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
$auth = function ($request, $response, $next)
{
    //Redireciona se o usuário já está logado
	if ($this->session->get('userSession')) {
		return $response->withRedirect($this->router->pathFor('homepage'));
	}else{
		return $next($request, $response);
	}
};

$guardian = function ($request, $response, $next)
{
    // Se o usuário não esiver logado, redirecionamos para o login.
    if(!$this->session->get('userSession')){
      $this->flash->addMessage('danger', 'Permissão Negada! Logue-se primeiro.');
      return $response->withRedirect($this->router->pathFor('login.index'));
    }

    // Dados da Rota
    $route = $request->getAttribute('route');
    // Nome da Rota
    $routeName = $route->getName();

    // Busco usuário através da sessão
    $login = $this->session->get('userSession');
    $user = $this->consulta->buscaUsuario($login);

    // Busco as as permissoes nos Settings
    $rotas = $this->get('settings')['auth'];

    // Verifico as rotas de acesso baseado na permissao do usuario
    foreach($rotas as $key => $value){
        if($user->getPapelId()->getTipo() == $key){
            if(in_array($routeName, $value)){
                return $next($request, $response);
            }else{
              $this->flash->addMessage('danger', 'Permissão Negada!');
              return $response->withRedirect($this->router->pathFor('homepage'));
            }
        }
    }
};