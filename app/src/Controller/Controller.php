<?php

namespace App\Controller;

/** 
 * Comentário de cabeçalho de arquivos
* Esta classe é uma classe abstrata para gerar o conteiner para as classes se controle
*
* @author José Felipe e Marcos Ereno
* @version 0.1 
*/


class Controller
{
	protected $container;

	public function __construct($container) // construtor para definir o uso do conteiner nas classes filhas
	{
		$this->container = $container;
	}

	public function __get($property) // pega todos os atributos que existem no conteiner de forma mágica
	{
		if($this->container->{$property})
		{
			return $this->container->{$property};
		}
	}
}