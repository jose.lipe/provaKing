<?php

namespace App\Action;

use Doctrine\Common\Util\Debug;
use App\Controller\Controller;
use Respect\Validation\Validator as v;

class SalaAction extends Controller
{
	// metodo que apresenta o cadastro de salas
	public function show($request, $response)
	{
		return $this->view->render($response, 'sala/sala.twig');
	}

	public function insert($request, $response)
	{
		$data = $request->getParsedBody();
		$this->resource->salvar('Sala',$data);
		$this->flash->addMessage('success', 'Sala cadastrada com sucesso!');
		$this->show($request, $response);
	}

	// Metodo que mostra todas as salas
	public function list($request, $response, $erros = null)
	{
		$salas = $this->consulta->buscaTodos('Sala');
		return $this->view->render($response, 'sala/list.twig', [
				'salas' => $salas,
				'errors' => $erros,
			]);
	}

	// Metodo que exibe a página de edição de sala
	public function edit($request,$response, $args, $erros = null)
	{	
		if (isset($args['id'])) {
			$sala = $this->consulta->buscaUm('Sala', $args['id']);
		}else{
			$sala = $this->consulta->buscaUm('Sala', $args);
		}
		return $this->view->render($response, 'sala/sala.twig', [
				'sala' => $sala,
				'errors' => $erros,
			]);
	}

	// Metodo de atualiza a sala
	public function update($request, $response, $args)
	{
		$data = $request->getParsedBody();
		$this->resource->salvar('Sala',$data, $args['id']);
		$this->flash->addMessage('success', 'Sala alterada com sucesso!');
		$this->list($request, $response);

	}

	// Metodo que deleta a sala
	public function delete($request, $response, $args)
	{
		// Testa se a sala possui reserva e as exclui
		$reservas = $this->consulta->buscaReservas($args['id']);
		if ($reservas) {
			foreach ($reservas as $reserva) {
				$this->resource->delete('Reserva', $reserva->getId());
			}	
		}

		$this->resource->delete('Sala', $args['id']);
		$this->flash->addMessage('success', 'Sala deletada com Sucesso!');
		$this->list($request, $response);
	}
}
