<?php

namespace App\Action;

use Doctrine\Common\Util\Debug;
use App\Controller\Controller;

class LoginAction extends Controller
{
	// Metodo para apresentar tela de Login
	public function show($request, $response)
	{
		return $this->view->render($response, 'login/index.twig');
	}

	// Metodo de logar
	public function logar($request, $response)
	{
		$data = $request->getParsedBody();
		$user = $this->consulta->buscaUsuario($data['login']);

		// Verifica se o usuário está cadastrado
		if (!$user) {
			$this->flash->addMessage('danger', 'Usuário não cadastrado!');
			return $this->show($request,$response);
		}

		// Teste se as senhas conferem
		if ($user->getSenha() == md5($data['senha'])) {
			//Seta variaveis de sessão
		 	$this->session->set('userSession', $user->getLogin());
		 	$this->session->set('userRole', $user->getPapelId()->getTipo());
			$this->session->set('userId', $user->getId());
			// Direciona para a página de home
		 	return $response->withRedirect($this->router->pathFor('homepage'));
		}

		//Erro caso a senha esteja incorreta
		$this->flash->addMessage('danger', 'Senha incorreta!');
		return $this->show($request,$response);
		
	}

	// Metodo para deslogar
	public function deslogar($request, $response)
	{
		$this->session::destroy();
		return $response->withRedirect($this->router->pathFor('login.index'));
	}
}