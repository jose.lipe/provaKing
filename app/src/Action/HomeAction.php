<?php

namespace App\Action;

use Doctrine\Common\Util\Debug;
use App\Controller\Controller;

class HomeAction extends Controller
{

	// Metodo para apresentar página inicial
	public function show($request, $response)
	{
		$salas = $this->consulta->buscaTodos('Sala');
		return $this->view->render($response, 'home/index.twig',[
				'salas' => $salas,
			]);

	}

}