<?php

namespace App\Action;

use Doctrine\Common\Util\Debug;
use App\Controller\Controller;
use Respect\Validation\Validator as v;

class RegisterAction extends Controller{

	// Metodo que apresenta a página de registro
	public function show($request, $response, $erros = null)
	{
		return $this->view->render($response, 'register/index.twig', [
				'errors' => $erros,
			]);
	}

	// Método que insere um novo usuário
	public function insert($request, $response)
	{
		$data = $request->getParsedBody();
		//Teste se o usuário já está cadastrado
		$user = $this->consulta->buscaUsuario($data['login']);
		if($user){
			$this->flash->addMessage('danger', 'Usuário já cadastrado, utilize outro.');
			return $this->show($request,$response);
		}

		try {
			
			// Valida tamanho da senha
			$this->validator->validate($request, [
				'senha' => v::length(6, 15),
			]);

			// caso seja válido insere novo user
			if ($this->validator->isValid()) {
				$this->resource->salvar('Usuario',$data);
				$this->flash->addMessage('success', 'Usuário cadastrado!');
				$this->show($request, $response);
			}else{
				throw new \Exception("A senha deve conter entre 6 a 15 caracteres!!");
				
			}


		} catch (\Exception $e) {
			$this->show($request, $response, $e->getMessage());
		}

	}

}