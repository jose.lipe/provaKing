<?php

namespace App\Action;

use Doctrine\Common\Util\Debug;
use App\Controller\Controller;
use Respect\Validation\Validator as v;

class UserAction extends Controller
{

	// Metodo que lista os usuários
	public function list($request, $response, $erros = null)
	{

		$users = $this->consulta->buscaTodos('Usuario');
		return $this->view->render($response, 'user/user.twig', [
				'users' => $users,
				'errors' => $erros,
			]);
	}

	// Metodo que mostra a página de edição de usuário
	public function edit($request,$response, $args, $erros = null)
	{	
		//Testa a forma de busca com argumanto
		if (isset($args['id'])) {
			$user = $this->consulta->buscaUm('Usuario', $args['id']);
		}else{
			$user = $this->consulta->buscaUm('Usuario', $args);
		}
		$papeis = $this->consulta->buscaTodos('Papel');
		return $this->view->render($response, 'user/edit.twig', [
				'papeis' => $papeis,
				'user' => $user,
				'errors' => $erros,
			]);
	}

	//Metodo que faz update de usuário
	public function update($request, $response, $args)
	{
		$data = $request->getParsedBody();
		$user = $this->consulta->buscaUm('Usuario', $args['id']);

		try {
			
			// valida o tamanho da senha
			$this->validator->validate($request, [
				'senha' => v::length(6, 15),
			]);

			// Se a senha é válida ele altera o usuário
			if ($this->validator->isValid()) {
				$this->resource->salvar('Usuario',$data, $user->getId());
				$this->flash->addMessage('success', 'Usuário alterado!');
				$this->list($request, $response);
			}else{
				throw new \Exception("A senha deve conter entre 6 a 15 caracteres!!");
				
			}

		} catch (\Exception $e) {
			$this->edit($request, $response, $args['id'], $e->getMessage());
		}

	}

	// Metodo que deleta o usuário
	public function delete($request, $response, $args)
	{

		// Testa se você está tentando se excluir
		if($args['id'] == $this->session->get('userId')){
			$this->flash->addMessage('danger', 'Você não pode excluir o seu Usuário');
			return $this->list($request, $response);
		}
		
		// Verifica se tem reservas no nome do usuário, e os deleta
		$reservas = $this->consulta->buscaReservasPorUsuario($args['id']);
		if ($reservas) {
			foreach ($reservas as $reserva) {
				$this->resource->delete('Reserva', $reserva->getId());
			}
		}

		$this->resource->delete('Usuario', $args['id']);
		$this->flash->addMessage('success', 'Usuário deletado com Sucesso!');
		$this->list($request, $response);
	}

}