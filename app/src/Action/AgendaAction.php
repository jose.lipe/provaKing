<?php

namespace App\Action;

use Doctrine\Common\Util\Debug;
use App\Controller\Controller;

class AgendaAction extends Controller
{
	// Metódo para aparecer a agenda de cada sala
	public function show($request, $response, $args)
	{
		// Teste sobre a forma busca de argumentos
		if (isset($args['id'])) {
			$sala = $this->consulta->buscaUm('Sala', $args['id']);
			$reservas = $this->consulta->buscaReservas($args['id']);
			$idSala = $args['id'];
		}else{
			$sala = $this->consulta->buscaUm('Sala', $args);
			$reservas = $this->consulta->buscaReservas($args);
			$idSala = $args;
		}
		
		// Renderizando a view
		return $this->view->render($response, 'agenda/agenda.twig',[
			'sala' => $sala,
			'reservas' => $reservas,
			'idsala' => $idSala,
		]);
	}

	// Metodo que apresenta a página de cadastro de novos eventos
	public function store($request,$response, $args, $erros = null)
	{
		// Teste sobre a forma busca de argumentos
		$horas = $this->consulta->buscaTodos('Horarios');
		if (isset($args['id'])) {
			$sala = $this->consulta->buscaUm('Sala', $args['id']);
			$idSala = $args['id'];
		}else{
			$sala = $this->consulta->buscaUm('Sala', $args);
			$idSala = $args;
		}

		// Renderizando a view
		return $this->view->render($response, 'agenda/edit.twig',[
			'sala' => $sala,
			'horas' => $horas,
			'errors' => $erros,
			'idSala' => $idSala,
		]);
	}

	// Método para inserir uma nova reserva
	public function insert($request, $response, $args)
	{
		$data = $request->getParsedBody();
		// Formata a data vinda da view
		$dataFormatada = str_replace('/', '-', $data['data']);
		// Gera uma nova data e hora de inicio
		$horainicio = new \DateTimeImmutable($dataFormatada." ".$data['hora']);
		// Calcula mais uma hora para a hora de inicio, gerando a hora final
		$horafim = $horainicio->add(new \DateInterval('PT1H'));

		$reservas = $this->consulta->buscaReservasPorDia($data['salaId'], new \DateTime($dataFormatada));
		try {
			// Teste para verificar se existe a hora que está tentando ser cadastrada no banco
			foreach ($reservas as $reserva) {
				if($reserva->getHorarioInicio() == $horainicio){
					throw new \Exception("Horário já reservado, verifique na agenda os horários disponíveis");
				}
			}

			// Preparação de array para inserção
			$insert = array(
				'horarioInicio' => $horainicio, 
				'horarioFim' => $horafim, 
				'motivo' => $data['motivo'], 
				'data' => new \DateTime($dataFormatada),
				'usuarioId' => $this->session->get('userId'),
				'salaId' => $data['salaId']
			);
			//insert no banco de dados
			$this->resource->salvar('Reserva',$insert);
			$this->flash->addMessage('success', 'Sala reservada com sucesso!');
			$this->show($request, $response, $data['salaId']);

		} catch (\Exception $e) {
			$this->flash->addMessage('danger', $e->getMessage());
			$this->store($request, $response, $data['salaId']);
		}
	}

	// Método para abrir edição de reserva
	public function edit($request,$response, $args, $erros = null)
	{	
		// Teste sobre a forma busca de argumentos
		$horas = $this->consulta->buscaTodos('Horarios');
		if (isset($args['id'])) {
			$reserva = $this->consulta->buscaUm('Reserva', $args['id']);
		}else{
			$reserva = $this->consulta->buscaUm('Reserva', $args);
		}

		// renderizando view
		return $this->view->render($response, 'agenda/edit.twig', [
				'horas' => $horas,
				'reserva' => $reserva,
				'errors' => $erros,
			]);
	}

	// Método para update de reservas
	public function update($request,$response, $args)
	{
		$data = $request->getParsedBody();
		// Formata a data vinda da view
		$dataFormatada = str_replace('/', '-', $data['data']);
		// Gera uma nova data e hora de inicio
		$horainicio = new \DateTimeImmutable($dataFormatada." ".$data['hora']);
		// Calcula mais uma hora para a hora de inicio, gerando a hora final
		$horafim = $horainicio->add(new \DateInterval('PT1H'));

		$reservas = $this->consulta->buscaReservasPorDia($data['salaId'], new \DateTime($dataFormatada));

		// Teste para verificar se existe a hora que está tentando ser cadastrada no banco
		try {
			foreach ($reservas as $reserva) {
				if($reserva->getHorarioInicio() == $horainicio){
					throw new \Exception("Horário já reservado, verifique na agenda os horários disponíveis");
				}
			}

			// Preparação de array para inserção
			$insert = array(
				'horarioInicio' => $horainicio, 
				'horarioFim' => $horafim, 
				'motivo' => $data['motivo'], 
				'data' => new \DateTime($dataFormatada),
				'usuarioId' => $this->session->get('userId'),
				'salaId' => $data['salaId']
			);
			//update no banco de dados
			$this->resource->salvar('Reserva',$insert, $args['id']);
			$this->flash->addMessage('success', 'Reservada alterada com sucesso!');
			$this->show($request, $response, $data['salaId']);

		} catch (\Exception $e) {
			$this->flash->addMessage('danger', $e->getMessage());
			$this->edit($request, $response, $args['id']);
		}
	}

	public function delete($request,$response, $args)
	{
		$reserva = $this->consulta->buscaUm('Reserva', $args['id']);
		$this->resource->delete('Reserva', $args['id']);
		$this->flash->addMessage('success', 'Reserva deletada com Sucesso!');
		$this->show($request, $response, $reserva->getSalaId()->getId());
	}

}