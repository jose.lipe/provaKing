<?php

namespace App\Resource;

/**
 * Comentário de cabeçalho de arquivos
* Metodos para recursos de objetos e banco de dados
*
* @author José Felipe e Marcos Ereno
* @version 0.1
*/

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Util\Debug;

class Resource
{
	/**
     * @var \Doctrine\ORM\EntityManager
     */

	protected $entityManager = null;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function insert($entity) // metodo que insere registros
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    public function delete($entity, $id) // metodo que deleta registros
    {
        $objeto = $this->entityManager->find('App\Entity\\'.$entity, $id);   
        $this->entityManager->remove($objeto);
        $this->entityManager->flush();
    }

    // Método que gera uma nova instancia de um objeto, que está dentro do namespace App\Entity
    public function loadEntity($classname){
        $entityInfo = $this->entityManager->getClassMetadata("App\Entity\\" . $classname);
        $entityMember = $entityInfo->newInstance();
        return $entityMember;
    }

    // Método que verifica relacionamento do atributo
    public function verificaRelacionamento($class, $atributo)
    {
    	$objeto = $this->entityManager->getClassMetadata("App\Entity\\" .$class);
    	if ($objeto->hasAssociation($atributo)) {
    		return $objeto;
    	}else{
    		return false;
    	}
    }

    // Método que verifica se o relacionamento é uma associação, composição ou agregação
    public function verificaTipoRelacionamento($obj, $atributo)
    {

        $tipo = null;

		$tipoAtributo = $obj->associationMappings[$atributo];
        if (@$tipoAtributo['joinTable']['inverseJoinColumns']) {
            return $tipo = "agregacao";
        }elseif(@$tipoAtributo['indexBy']){
            return $tipo = "composicao";
        }else{
            return $tipo = "associacao";
        }
    }

    // Metodo que preenche um método set
    public function preencheMetodoSemRelacionamento($objeto, $atributo, $valor)
    {
        $atr = ucfirst($atributo);
        $metodo = "set".$atr;
        return $objeto->$metodo($valor);
    }

    // Método que preenche um método de associação
    public function preencherAssociacao($objeto, $atributo, $valor, $class)
    {
        $entidade = $this->entityManager->getClassMetadata("App\Entity\\" .$class);
        $relacionamento = $entidade->getAssociationMappings();
        if (array_key_exists($atributo, $relacionamento)) {
            $namespace = $relacionamento[$atributo]["targetEntity"];
        }
        $entidadeRelacionada = $this->entityManager->find($namespace, $valor);

        $atr = ucfirst($atributo);
        $metodo = "set".$atr;
        return $objeto->$metodo($entidadeRelacionada);
    }

    // Método que preenche uma agregação
    public function preencherAgregacao($objeto, $atributo, $valor, $class)
    {
    	$atr = ucfirst($atributo);
        $metodo = "add".$atr;
        $metodoBusca = "get".$atr;
        $metodoRemove = "remove".$atr;

        // verifica se o objeto já tem alguma agregação, se tiver ele exclui para dar update
    	if(count($objeto->$metodoBusca()) != 0){
    		foreach ($objeto->$metodoBusca() as $rmv) {
                $objeto->$metodoRemove($rmv);
            }

            $this->insert($objeto);
    	}

        $entidade = $this->entityManager->getClassMetadata("App\Entity\\" .$class);
        $relacionamento = $entidade->getAssociationMappings();
        if (array_key_exists($atributo, $relacionamento)) {
            $namespace = $relacionamento[$atributo]["targetEntity"];
        }
        $entidadeRelacionada = $this->entityManager->find($namespace, $valor);

        return $objeto->$metodo($entidadeRelacionada);
    }

    // Método que preenche uma composição
    public function preencherComposicao($objeto, $atributo, $valor, $class)
    {

        $atr = ucfirst($atributo);
        $metodo = "add".$atr;
        $metodoBusca = "get".$atr;

        // verifica se o objeto já tem alguma composição, se tiver ele exclui para dar update
        if (count($objeto->$metodoBusca()) != 0) {
            foreach ($objeto->$metodoBusca() as $objBuscado) {
                $this->entityManager->remove($objBuscado);
                $this->entityManager->flush();   
            }
        }

        $teste = new \ReflectionMethod("App\Entity\\" . $class, $metodo);
        $args = array();

        foreach ($valor as $c => $v) {
           foreach ($teste->getParameters() as $param) {
                if($c == $param->getName()){
                    if (substr($param->getName(), -2) == 'Id') {
                        $busca = substr($param->getName(), 0, -2);
                        $buscaFormadata = $atr = ucfirst($busca);
                        $entidade = $this->entityManager->find('App\Entity\\'.$buscaFormadata, $v);
                        $args[] = $entidade;
                    }else{
                        $args[] = $v;
                    }
                }
            }

        }
        
        return call_user_func_array(array($objeto,$metodo), $args);
    }


    // Metodo que preenche um objeto, se o mesmo ainda não existir ou gera um updade caso já exista.
    public function salvar($class, $data,$id = null)
    {

        if ($id) {
            $objeto = $this->entityManager->find('App\Entity\\'.$class, $id);
        }else{
            $objeto = $this->loadEntity($class);
        }
        foreach ($data as $atributo => $valor) {
            $obj = $this->verificaRelacionamento($class, $atributo);

            if ($obj) {
                $tipo = $this->verificaTipoRelacionamento($obj, $atributo);
                if ($tipo == 'associacao') {
                    $this->preencherAssociacao($objeto, $atributo, $valor, $class);
                }else if($tipo == 'agregacao'){
                    $this->preencherAgregacao($objeto, $atributo, $valor, $class);
                }else{
                    $this->preencherComposicao($objeto, $atributo, $valor, $class);
                }   
            }else{
                $this->preencheMetodoSemRelacionamento($objeto, $atributo, $valor);
            }
        }

        $this->insert($objeto);

    }

}
