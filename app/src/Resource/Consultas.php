<?php

namespace App\Resource;

/**
 * Comentário de cabeçalho de arquivos
* Esta classe guarda os metodos de uso de banco de dados
*
* @author José Felipe e Marcos Ereno
* @version 0.1
*/

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Util\Debug;


class Consultas
{

	/**
     * @var \Doctrine\ORM\EntityManager
     */

	protected $entityManager = null;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

	public function buscaUm($entity, $id) // buscar um registro de uma entidade
    {
    	return $objeto = $this->entityManager->find('App\Entity\\'.$entity, $id);
    }


    public function buscaTodos($entity) // busca todos os registros
    {
    	return $objetos = $this->entityManager->getRepository('App\Entity\\'.$entity)->findAll();
    }

    public function buscaUsuario($login) // Busca usuário pelo seu login
    {
        return $objetos = $this->entityManager->getRepository('App\Entity\Usuario')->findOneBy(array('login' => $login));
    }

    public function buscaReservas($salaId) // buscas as reservas de uma sala
    {
        return $objeto = $this->entityManager->getRepository('App\Entity\Reserva')->findBy(array('salaId' => $salaId));
    }

    public function buscaReservasPorDia($salaId, $data) // Busca as reservas pela sala e por dia
    {
        return $objeto = $this->entityManager->getRepository('App\Entity\Reserva')->findBy(array('data' => $data, 'salaId' => $salaId));
    }

    public function buscaReservasPorUsuario($usuarioId) // Busca as reservas pelo usuário
    {
        return $objeto = $this->entityManager->getRepository('App\Entity\Reserva')->findBy(array('usuarioId' => $usuarioId));
    }
}