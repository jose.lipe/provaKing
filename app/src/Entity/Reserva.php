<?php

namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="reserva")
 */
class Reserva
{
	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column( name="horario_inicio" ,type="datetime")
	 */
	private $horarioInicio;

	/**
	 * @ORM\Column( name="horario_fim" ,type="datetime")
	 */
	private $horarioFim;

	/**
	 * @ORM\Column(type="text")
	 */
	private $motivo;

	/**
	 * @ORM\Column(type="date")
	 */
	private $data;

	/**
	 * Many Reserva have One Usuario
	 * @ORM\ManyToOne(targetEntity="Usuario")
	 * @ORM\JoinColumn(name="usuario_id",referencedColumnName="id")
	 */
	private $usuarioId;

	/**
	 * Many Reserva have One Sala
	 * @ORM\ManyToOne(targetEntity="Sala")
	 * @ORM\JoinColumn(name="sala_id",referencedColumnName="id")
	 */
	private $salaId;

	/* Get id
	*
	* @return string
	*/
	public function getId()
	{
	    return $this->id;
	}

	public function getHorarioInicio()
	{
	    return $this->horarioInicio;
	}
	 
	public function setHorarioInicio($horarioInicio)
	{
	    $this->horarioInicio = $horarioInicio;
	    return $this;
	}

	public function getHorarioFim()
	{
	    return $this->horarioFim;
	}
	 
	public function setHorarioFim($horarioFim)
	{
	    $this->horarioFim = $horarioFim;
	    return $this;
	}

	public function getMotivo()
	{
	    return $this->motivo;
	}
	 
	public function setMotivo($motivo)
	{
	    $this->motivo = $motivo;
	    return $this;
	}

	public function getData()
	{
	    return $this->data;
	}
	 
	public function setData($data)
	{
	    $this->data = $data;
	    return $this;
	}

	public function getUsuarioId()
	{
	    return $this->usuarioId;
	}
	 
	public function setUsuarioId(\App\Entity\Usuario $usuarioId)
	{
	    $this->usuarioId = $usuarioId;
	    return $this;
	}

	public function getSalaId()
	{
	    return $this->salaId;
	}
	 
	public function setSalaId(\App\Entity\Sala $salaId)
	{
	    $this->salaId = $salaId;
	    return $this;
	}
}