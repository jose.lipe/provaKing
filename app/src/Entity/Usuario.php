<?php

namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuario")
 */
class Usuario
{
	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=30)
	 */
	private $login;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $email;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $senha;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $nome;

	/**
	 * Many Usuario have One Papel
	 * @ORM\ManyToOne(targetEntity="Papel")
	 * @ORM\JoinColumn(name="papel_id",referencedColumnName="id")
	 */
	private $papelId;

	/* Get id
	*
	* @return string
	*/
	public function getId()
	{
	    return $this->id;
	}

	public function getLogin()
	{
	    return $this->login;
	}
	 
	public function setLogin($login)
	{
	    $this->login = $login;
	    return $this;
	}

	public function getEmail()
	{
	    return $this->email;
	}
	 
	public function setEmail($email)
	{
	    $this->email = $email;
	    return $this;
	}

	public function getSenha()
	{
	    return $this->senha;
	}
	 
	public function setSenha($senha)
	{
	    $this->senha = md5($senha);
	    return $this;
	}

	public function getNome()
	{
	    return $this->nome;
	}
	 
	public function setNome($nome)
	{
	    $this->nome = $nome;
	    return $this;
	}

	public function setPapelId(\App\Entity\Papel $papelId){
        $this->papelId = $papelId;
    }

    public function getPapelId(){
        return $this->papelId;
    }
}