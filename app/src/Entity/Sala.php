<?php

namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sala")
 */
class Sala
{

	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $numero;

	/**
	 * @ORM\Column(type="text")
	 */
	private $descricao;

	/* Get id
	*
	* @return string
	*/
	public function getId()
	{
	    return $this->id;
	}

	public function getNumero()
	{
	    return $this->numero;
	}
	 
	public function setNumero($numero)
	{
	    $this->numero = $numero;
	    return $this;
	}

	public function getDescricao()
	{
	    return $this->descricao;
	}
	 
	public function setDescricao($descricao)
	{
	    $this->descricao = $descricao;
	    return $this;
	}
}