<?php

namespace App\Entity;

use App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="horarios")
 */
class Horarios
{
	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=5)
	 */
	private $hora;

	/* Get id
	*
	* @return string
	*/
	public function getId()
	{
	    return $this->id;
	}

	public function getHora()
	{
	    return $this->hora;
	}
	 
	public function setHora($hora)
	{
	    $this->hora = $hora;
	    return $this;
	}
}