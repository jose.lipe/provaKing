<?php
return [
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true,
 
        // View settings
        'view' => [
            'template_path' => __DIR__ . '/templates',
            'twig' => [
                'cache' => __DIR__ . '/../cache/twig',
                'debug' => true,
                'auto_reload' => true,
            ],
        ],

        // Permissão dos Usuarios por Rotas
        'auth' => [
            'usuario' => [
                'homepage',
                'sala.show',
                'sala.insert',
                'sala.list',
                'sala.edit',
                'sala.update',
                'sala.delete',
                'agenda.show',
                'agenda.store',
                'agenda.edit',
                'agenda.insert',
                'agenda.update',
                'agenda.delete'
            ],
            'admin' => [
                'homepage',
                'user.list',
                'user.edit',
                'user.update',
                'user.delete',
                'sala.show',
                'sala.insert',
                'sala.list',
                'sala.edit',
                'sala.update',
                'sala.delete',
                'agenda.show',
                'agenda.store',
                'agenda.edit',
                'agenda.insert',
                'agenda.update',
                'agenda.delete'
            ],
        ],
 
        // Monolog Settings
        'logger' => [
            'name' => 'app',
            'path' => __DIR__ . '/../log/app.log',
        ],
 
        // PHPMailer
        'phpmailer' => [
            'host' => 'smtp.kinghost.net',
            'SMTPAuth' => true,
            'Username' => 'josefelipe@jose.felipe.kinghost.net',
            'Password' => 'rada85',
            'SMTPSecure' => 'tls',
            'Port' => 587,
            'isHTML' => true,
            'setFrom' => 'josefelipe@jose.felipe.kinghost.net',
        ],
 
        // Doctrine
        'doctrine' => [
            'meta' => [
                'entity_path' => [
                    'app/src/Entity'
                ],
                'auto_generate_proxies' => true,
                'proxy_dir' => __DIR__.'/../cache/proxies',
                'cache' => null,
            ],
            'connection' => [
                'driver' => 'pdo_mysql',
                'host' => 'host',
                'dbname' => 'database',
                'user' => 'usuario',
                'password' => 'senha', //g0o7k9i1n0g
                'charset' => 'utf8mb4',
                'collate' => 'utf8mb4_general_ci',
            ]
        ]
 
    ],
];