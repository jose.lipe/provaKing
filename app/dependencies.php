<?php
// DIC configuration

$container = $app->getContainer(); // gera um container

// -----------------------------------------------------------------------------
// Provedores de serviço
// -----------------------------------------------------------------------------

// Twig
$container['view'] = function ($c) { // serviço de renderizador de templates em HTNL
    $settings = $c->get('settings');
    $view = new Slim\Views\Twig($settings['view']['template_path'], $settings['view']['twig']);

    // Add extensões do Twig
    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $c->get('request')->getUri()));
    $view->addExtension(new Twig_Extension_Debug());
    // Add the validator extension
    $view->addExtension(new Awurth\SlimValidation\ValidatorExtension($c->get('validator')));
    // Add Flash
    $view->addExtension(new Knlv\Slim\Views\TwigMessages(new Slim\Flash\Messages()));
    // Declarado como variável global a sessao do usuario
    $view->getEnvironment()->addGlobal('sessao', $_SESSION);


    return $view;
};

// Flash messages
$container['flash'] = function ($c) { // serviço de mensagens flash
    return new Slim\Flash\Messages;
};

// Session
$container['session'] = function ($c) {
    return new RKA\Session;
};

// PHPMailer
$container['mail'] = function ($c) { // serviço de envio de email
    
    $settings = $c->get('settings');
    
    $object = new PHPMailer;
        
    $object->isSMTP();

    $object->Host       = $settings['phpmailer']['host']; 
    $object->SMTPAuth   = $settings['phpmailer']['SMTPAuth'];                              
    $object->Username   = $settings['phpmailer']['Username'];                  
    $object->Password   = $settings['phpmailer']['Password'];                           
    $object->SMTPSecure = $settings['phpmailer']['SMTPSecure'];                             
    $object->Port       = $settings['phpmailer']['Port'];     
    
    $object->setFrom($settings['phpmailer']['setFrom'], 'GO KING');     

    $object->isHTML($settings['phpmailer']['isHTML']); 
    $object->CharSet = 'UTF-8';   
    
    return $object;
};

$container['validator'] = function () {
    return new Awurth\SlimValidation\Validator();
};



// -----------------------------------------------------------------------------
// Fábrica de Serviços
// -----------------------------------------------------------------------------

// Monolog
$container['logger'] = function ($c) { //cria o serviço de logger do Slim
    $settings = $c->get('settings');
    $logger = new Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['logger']['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Doctrine
$container['em'] = function ($c) { //cria o serviço de Configuração do Doctrine
    $settings = $c->get('settings');
    $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
            $settings['doctrine']['meta']['entity_path'],
            $settings['doctrine']['meta']['auto_generate_proxies'],
            $settings['doctrine']['meta']['proxy_dir'],
            $settings['doctrine']['meta']['cache'],
            false
        );
        return \Doctrine\ORM\EntityManager::create($settings['doctrine']['connection'], $config);    
};

$container['resource'] = function ($c) { 
    return new \App\Resource\Resource($c->get('em'));
};

$container['consulta'] = function ($c) { 
    return new \App\Resource\Consultas($c->get('em'));
};




